/* Array list of products */

var items = [ {
        id : 1
        , productName : "Acer Aspire E1-572"
        , price : 399.00
        , os : "Windows 8.1"
        , dimensions : "380 x 27 x 255"
        , weight : 2.193
        , transportWeight : 2.47
        , processor : "Intel Core i5-4200U"
        , processorSpeed : 1.6
        , ram : 4
        , storageType : "Spinning HD 5400"
        , storage : 500
    },
    {
        id : 2
        , productName : "Acer Aspire S3-392G"
        , price : 899.99
        , os : "Windows 8.1"
        , dimensions : "221 x 17 x 124"
        , weight : 1.618
        , transportWeight : 1.97
        , processor : "Intel Core i5-4200U"
        , processorSpeed : 1.6
        , ram : 4
        , storageType : "Spinning HD 5400"
        , storage : 500
    },
    {
        id : 3
        , productName : "Acer Aspire V5-573"
        , price : 599.95
        , os : "Windows 8.1"
        , dimensions : "377 x 23 x 255"
        , weight : 2.045
        , transportWeight : 2.46
        , processor : "Intel Core i7-4500U"
        , processorSpeed : 1.8
        , ram : 8
        , storageType : "Spinning HD 5400"
        , storage : 1000
    },
    {
        id : 4
        , productName : "Acer V5-552"
        , price : 479.99
        , os : "Windows 8.0"
        , dimensions : "378 x 24 x 254"
        , weight : 2
        , transportWeight : 2.41
        , processor : "AMD A10-5757M"
        , processorSpeed : 2.1
        , ram : 6
        , storageType : "Spinning HD 5400"
        , storage : 1000
    },
    {
        id : 5
        , productName : "Apple MacBook Air 11-inch (2013)"
        , price : 585.00
        , os : "OSX 10.8.4"
        , dimensions : "299 x 13.4 x 192"
        , weight : 1.079
        , transportWeight : 1.39
        , processor : "Intel Core i5-4250U"
        , processorSpeed : 1.3
        , ram : 4
        , storageType : "SSD"
        , storage : 128
    },
    {
        id : 6
        , productName : "Apple MacBook Air 13-inch (2013)"
        , price : 669.99
        , os : "OSX 10.8.4"
        , dimensions : "325 x 12.6 x 227"
        , weight : 1.328
        , transportWeight : 1.64
        , processor : "Intel Core i5-4250U"
        , processorSpeed : 1.3
        , ram : 4
        , storageType : "SSD"
        , storage : 128
    },
    {
        id : 7
        , productName : "Apple Macbook Pro Retina 13-inch (2013)"
        , price : 856.39
        , os : "Mac OS 10.9"
        , dimensions : "314 x 17.8 x 219"
        , weight : 1.566
        , transportWeight : 1.94
        , processor : "Intel Core i5-4258U"
        , processorSpeed : 2.4
        , ram : 4
        , storageType : "SSD"
        , storage : 128
    },
    {
        id : 8
        , productName : "Apple Macbook Pro Retina 15-inch (2013)"
        , price : 1338.00
        , os : "Mac OS 10.9"
        , dimensions : "359 x 18 x 247"
        , weight : 1.997
        , transportWeight : 2.37
        , processor : "Intel Core i7-4750HQ"
        , processorSpeed : 2
        , ram : 8
        , storageType : "SSD"
        , storage : 256
    }
            ];