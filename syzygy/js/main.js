/* global vars */
var itemIndex;
var itemClass;
var itemHighlighted;

$(document).ready(function() {
   
    /* item injection on all selections */
    for(i=0; i< items.length; i++) {
        $(".select").append('<option value="' + items[i].id + '">' + items[i].productName + '</option>');
        $(".tabs .child").append('<li><a href="javascript:void(0);" rel="' + items[i].id + '">' + items[i].productName + '</a></li>');
    }
    
    /* on selection switch */
    
    /* desktop version */
    $('select').change(function(){
        itemIndex = $(this).val() -1;
        itemClass = $(this).attr("class");
        
        /* if the content not contains the title results */
        if($("#content h1").length <= 0) {
            $("#content").prepend('<h1>Results:</h1>');
        }
        contupdate();
        
    });
    /* mobile version */
    $(".tabs li a").on("click", function() {
        if($(".child").is(":visible")) {
            $(".child").hide(); 
            $(this).next(".child").fadeIn(800);
        } else {
            $(this).next(".child").slideDown(800);
        }
    });
    
    $(".child li a").on("click", function() {
        itemIndex = $(this).attr('rel') -1;
        itemClass = $(this).closest(".child").prev("a").attr("class");

        contupdate();  

    });
    
    /* click listener on dynamic created elements  */
    $('#content').on('click', '.prev', function (e) {
        $("#" + itemHighlighted).hide();

        itemHighlighted = $("#" + itemHighlighted).parent().prev().children(".resultblock").attr("id");
        
        $("#" + itemHighlighted).fadeIn();
        
        /* arrow management on the fly */
        if($.trim($("#" + itemHighlighted).parent().prev().children(".resultblock").html()) != ''  ) {
            $(this).show();
            $(".next").show();
        } else {
            $(this).hide();
            $(".next").show();
        }
    });     
    
    $('#content').on('click', '.next', function (e) {
        $("#" + itemHighlighted).hide();

        itemHighlighted = $("#" + itemHighlighted).parent().next().children(".resultblock").attr("id");
        
        $("#" + itemHighlighted).fadeIn();
        
        /* arrow management on the fly */
        if($.trim($("#" + itemHighlighted).parent().next().children(".resultblock").html()) != ''  ) {
            $(this).show();
            $(".prev").show();
        } else {
            $(this).hide();
            $(".prev").show();
        }
        

    });
    
    /* on resize event */
    $(window).resize(function() {
        resizeManager();
    });
});
 
/* function expression loaded when is needed only */

var contupdate = function() {
    
        /* create structure of item sheet */
        var output = '<h2>' + items[itemIndex].productName + '</h2>';
        output += '<p>' + items[itemIndex].price + '</p>';
        output += '<p class="negative">' + items[itemIndex].os + '</p>';
        output += '<p>' + items[itemIndex].dimensions + '</p>';
        output += '<p class="negative">' + items[itemIndex].weight + '</p>';
        output += '<p>' + items[itemIndex].transportWeight + '</p>';
        output += '<p class="negative">' + items[itemIndex].processor + '</p>';
        output += '<p>' + items[itemIndex].processorSpeed + '</p>';
        output += '<p class="negative">' + items[itemIndex].ram + '</p>';
        output += '<p>' + items[itemIndex].storageType + '</p>';
        output += '<p class="negative">' + items[itemIndex].storage + '</p>';
        
        /* check what selection has been selected and check for the same existing result */
    
        if(itemClass.indexOf("first") >= 0) {
            itemHighlighted = "firstChoice";
            if(output == $("#secondChoice").html() || output == $("#thirdChoice").html()) {
                $("#firstChoice").html('<p class="alert">You can\'t compare a product with itself<br/>make another choice</p>');
            } else {
                $("#firstChoice").html(output);
            }
            $("#firstChoice").fadeIn();

        } else if(itemClass.indexOf("second") >= 0) {
            itemHighlighted = "secondChoice";
            if(output == $("#firstChoice").html() || output == $("#thirdChoice").html()) {
                $("#secondChoice").html('<p class="alert">You can\'t compare a product with itself<br/>make another choice</p>');
            } else {
                $("#secondChoice").html(output);
            }
            $("#secondChoice").fadeIn();

        } else {
            itemHighlighted = "thirdChoice";
            if(output == $("#firstChoice").html() || output == $("#secondChoice").html()) {
                $("#thirdChoice").html('<p class="alert">You can\'t compare a product with itself<br/>make another choice</p>');
            } else {
                $("#thirdChoice").html(output);
            }
            $("#thirdChoice").fadeIn();
            
        }
        navCreator();
        
     
};

var navCreator = function() {
        /* navigation tabs only */
        if($(".tabs").is(":visible")) {         
            var countResults = $(".resultblock:visible").length;
            
            $(".resultblock:not(#" + itemHighlighted + ")").hide();
            
            /* if results are more than 1 create a navigation */
            if(countResults > 1) {
                if(!$(".nav").length) {
                    $("#content").prepend('<ul class="nav"><li><a href="javascript:void(0);" class="prev notvisible"><</a></li><li><a href="javascript:void(0);" class="next notvisible">></a></li></ul>');
                }
            } 
            
            /* nav arrows visibility management */
            setArrowsVisibility();
        }
};

var setArrowsVisibility = function() {

    if($.trim($("#" + itemHighlighted).parent().prev().children(".resultblock").html()) != ''  ) {
        $(".prev").fadeIn();
    } else {
        $(".prev").hide();
    }

    if($.trim($("#" + itemHighlighted).parent().next().children(".resultblock").html()) != ''  ) {
        $(".next").fadeIn();
    } else {
        $(".next").hide();

    }
};

var resizeManager = function() {
    if($(".tabs").is(":visible")) {
        $(".resultblock:not(#" + itemHighlighted + ")").hide();
        navCreator();

    } else {
        $(".nav a").hide();
        $(".resultblock").each(function() {
            if($.trim($(this).html()) != "") {
                $(this).show();
            }
        });
    }
};